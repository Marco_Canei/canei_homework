package Indovina_numero2;

import java.io.*;
import java.net.*;

import javax.swing.*;

public class Client {
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private Socket connessione;
	private int tentativi=0;
	private String nome,risultato,s;
	public boolean indovinato=false;
	public int n=((int)Math.random())*100;
	
	public Client(String nome){
		this.nome=nome;
		System.out.println(nome+" tenta di connettersi");
	
		try {
			connessione=new Socket(InetAddress.getByName("localhost"), 12345);
			output=new ObjectOutputStream(connessione.getOutputStream());
			output.flush();
			input=new ObjectInputStream(connessione.getInputStream());
			while(indovinato!=true	&&	tentativi<10 ){
				
				 risultato=(String) input.readObject();
				 if(risultato.equals("GET")){
					 s=JOptionPane.showInputDialog(nome+" prova ad indovinare");
					 output.writeObject(s);
					 tentativi++;
				 }
				 //risultato=(String) input.readObject();
				 else if(risultato.equals("WON")){
					 JOptionPane.showMessageDialog(null, nome+" hai vinto!!");
					 indovinato=true;
				 }
				 else if(risultato.equals("LOST")){
					 JOptionPane.showMessageDialog(null, nome+" hai perso!!");
					 indovinato=true;
				 }
				 else if(risultato.equals("DRAW")) {
					 JOptionPane.showMessageDialog(null, "avete pareggiato!!");
					 indovinato=true;
				 }
				
				
				
			}
			}catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally{
			System.out.println(nome+" chiude connessione");
			try {
				if(connessione != null) 
					connessione.close();
				if(output != null) 
					output.close();
				if(input != null)
					input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	public static void main(String args[]){
		new Client("Client1");
		
		
		}
		
		
	}



