package Teatro;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client extends Thread {
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private Socket connessione;
	private String nome;
	private int scelta,i,j,n;
	private Scanner tastiera;
	private int occupa[]=new int[300],posti[]=new int[300];
	private int prenotazione[]=new int[300];
	
	public boolean isEmpty(int[]n){
		int x=0;
		i=0;
		x=n[i];
		while((x==0	&&	i+1<300	)	&&	x==n[i+1]		){
			i++;
			x=n[i];
			
		}
		if(i!=299)
			return false;
		
		return true;
		
		}
	public void ordina(int [] array) {

        for(int i = 0; i < array.length; i++) {
            boolean flag = false;
            if(array[i]!=0){
            for(int j = 0; j < array.length-1; j++) {

                //Se l' elemento j e maggiore del successivo allora
                //scambiamo i valori
            	
                if(array[j]>array[j+1]	) {
                    int k = array[j];
                    array[j] = array[j+1];
                    array[j+1] = k;
                    flag=true; //Lo setto a true per indicare che � avvenuto uno scambio
                }
                

            }
            }

            if(!flag)
            	break; 
        	}
            //Se flag=false allora vuol dire che nell' ultima iterazione
            //non ci sono stati scambi, quindi il metodo pu� terminare
            //poich� l' array risulta ordinato
        }
    
	public Client(String nome){
		System.out.println("client");
		this.nome=nome;
		System.out.println("client tenta di connettersi");
		try {
			connessione=new Socket(InetAddress.getByName("localhost"), 12345);
			output=new ObjectOutputStream(connessione.getOutputStream());
			output.flush();
			input=new ObjectInputStream(connessione.getInputStream());
			tastiera=new Scanner(System.in);
			System.out.println("inserisci nominativo");
			output.writeObject(tastiera.nextLine());
			
			do{
				System.out.println("cosa vuoi fare?");
				System.out.println("1)Richiesta posti liberi ");
				System.out.println("2)Richiesta prenotazione");
				System.out.println("3)Richiesta posti prenotati");
				System.out.println("4)Annullamento prenotazione");
				System.out.println("5)Chiudi connessione");
				System.out.println("0)termina");
				scelta=tastiera.nextInt();
				output.writeObject(scelta);
				for(i=0;i<300;i++)
					posti[i]=i;
				switch(scelta){
				case 1:				
					occupa=(int[]) input.readObject();
					if(isEmpty(occupa)==true){
						for(i=0;i<300;i++){
							System.out.print(posti[i]+" ");
							if(	i==30	||	i==60	||	i==90	||	i==120	||	i==150	||	i==180	||	i==210	||	i==240	||	i==270	||	i==299	)
								System.out.println(" ");
						}
					}
					else{
						
					
						
						System.out.println("posti liberi:");					
							for(j=0;j<occupa.length;j++){
								for(i=0;i<300;i++)
										if(posti[i]==occupa[j])								
											posti[i]=-1;				
							}						
							for(i=0;i<300;i++){
								if(posti[i]!=-1)
									System.out.print(posti[i]+" ");					
								if(	i==30	||	i==60	||	i==90	||	i==120	||	i==150	||	i==180	||	i==210	||	i==240	||	i==270	||	i==299	)
									System.out.println(" ");
							}
						
						}
							break;
				case 2:				
					//GUI g=new GUI();
					int t[]=new int[300];
					t=new GUI().prenota();
					/*System.out.println("quali posti vuoi prenotare? -1 per terminare");
					i=0;
					n=0;		
					prenotazione=new int[300];
					while(i<300 && n!=-1){					
						if(prenotazione[i]==0){
							n=tastiera.nextInt();
							if(n!=-1)
								prenotazione[i]=n;
							else								
								break;		
						}
						i++;
					}	
					for(j=i;j<300;j++)
						prenotazione[j]=-1;*/
					
					for(i=0;i<300;i++)
					System.out.print(t[i]+" ");
					output.writeObject(prenotazione);				
					break;
					
				case 3:					
					int x[]=new int[300];
					x=(int [])input.readObject();
					System.out.println(" ");
					System.out.println("hai prenotato i posti:");
					for(i=0;i<x.length;i++)
						if(x[i]!=-1)
						 System.out.print(x[i]+" ");
							System.out.println(" ");
					break;
					
				case 4:
					System.out.println("quali posti vuoi distire? -1 per terminare  ");
					int a[]=new int[300];
					i=0;n=0;
							
					while(i<300 && n!=-1){
						n=tastiera.nextInt();
						a[i]=n;
						i++;
					}
					output.writeObject(a);
					System.out.println(input.readObject());
					break;
				
				case 5:
					System.out.println(nome+" chiude connessione");
					
						if(connessione != null){ 
							connessione.close();
							scelta=0;
						}
					
					break;
				
				}
			}while(scelta!=0);
			
			
			
			
			
			
		} catch (UnknownHostException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {			
			e.printStackTrace();
		}
		finally{
			try {
				if(connessione != null) 
					connessione.close();
				if(output != null) 
					output.close();
				if(input != null)
					input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		
	}
	public static void main(String[] args) {
		new Client("Client");
	}
	
}


