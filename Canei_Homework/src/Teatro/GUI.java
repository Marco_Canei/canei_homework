package Teatro;



import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.*;

import javax.swing.*;


public class GUI  	extends JPanel implements MouseListener  {
	
	
	public static JLabel immagini[]=new JLabel[300];
	private boolean occupato[]=new boolean[300];
	public ImageIcon liberoI=new ImageIcon("img/libero.png");
	public ImageIcon occupatoI=new ImageIcon("img/occupato_da_me.png");
	private JButton invia=new JButton("invia");
	private boolean LIBERO=true, OCCUPATO=false;
	private boolean stato=LIBERO;
	private int i,j;
	private int[]prenotazioni=new int[300];
	
	public int[] prenota(){
		j=0;
		
		for(i=0;i<300;i++){
			if(immagini[i].getIcon().toString().equals("img/occupato_da_me.png")){
				prenotazioni[j]=i;
				j++;
			}
		}
		for(i=0;i<300;i++)
			System.out.print(prenotazioni[i]+" ");
		return prenotazioni;
	}
	
	public GUI(){
		JFrame f=new JFrame();
		JPanel p1=new JPanel();
		JPanel p=new JPanel(new GridLayout(6,50));
		
		for(int i=0;i<300;i++){
			immagini[i]=new JLabel();
			immagini[i].setIcon(liberoI);
			immagini[i].addMouseListener(this);
			p.add(immagini[i]);
		}
		
		//p1.add(p);
		invia.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				//prenota();
				
			}
		});
		p.add(invia,BorderLayout.SOUTH);
		f.add(p);
		f.setVisible(true);
		f.setSize(1800, 400);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		
		
	}

	

	@Override
	public void mouseClicked(MouseEvent g) {
		JLabel l=(JLabel)g.getSource();
		if(l.getIcon().toString().equals("img/libero.png"))
		l.setIcon(occupatoI); 
		else
			l.setIcon(liberoI);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
public static void main(String[] args) {
	new GUI();
	
	
	
}}

