package Teatro;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server extends Thread{
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket server;
	private Socket c;
	private int posti[]=new int[300],lettura[]=new int[300];//posti 
	private String occupato[]=new String[300];//posti occupati
	private int prenotazioni[]=new int[300];
	private String nominativo,decisione;
	private boolean stop=false,termina=false;
	private int scelta, i,j,k=0;
	private Scanner tastiera;
	
	
	
	public Server(){
		System.out.println("server");
	try {
		server=new ServerSocket(12345);
		
		
		
		for(i=0;i<300;i++){
			posti[i]=i;
			occupato[i]=null;
			prenotazioni[i]=-1;
		}
		do{
			System.out.println("server tenta di connettersi");
			c=server.accept();
			output=new ObjectOutputStream(c.getOutputStream());
			input=new ObjectInputStream(c.getInputStream());
			output.flush();
			tastiera=new Scanner(System.in);
		nominativo=(String)input.readObject();
		
		 
		 while(stop==false){
			
			 scelta=(int) input.readObject();
			
			 switch(scelta){
			 case 1:
				 int x[]=new int[300];
				 j=0;
				 for(i=0;i<300;i++)
					 if(occupato[i]!=null){
						 x[j]=posti[i];
						 j++;
					 }			 
				
				 output.writeObject(x);
				 break;
					 
			 case 2:	
				lettura=(int [])input.readObject();
				int lettura1[]=new int[300];
				for(i=0;i<lettura1.length;i++)
					System.out.print(lettura1[i]+" ");
				System.out.println(" ");
			 for(i=0;i<300;i++){
					 for(j=0;j<300;j++){
						 if(lettura[j]==i	&& lettura[j]!=-1){
							 if(occupato[i]==null){
								 prenotazioni[k]=i;
								 occupato[i]=nominativo;
								 k++;
								 break;
							 }
							 else{
								 System.out.println("non puoi prenotare questi posti "+i);
								 for(i=0;i<300;i++)
									 occupato[i]=null;
								 break;
							 }
						 }
						 
					 }	
				 }
				 
				 break;
				 
			 case 3:
				 int t[]=new int[300];
				 k=0;
				 for(i=0;i<300;i++){
					 if(occupato[i]==nominativo){
						 t[k]=prenotazioni[i];
						 k++;
					 }
				 }
				 for(i=k;i<300;i++)
					t[i]=-1;
				 output.writeObject(t);				 
				 break;	
				 
			 case 4:
				 int a[]=new int[300];
				 j=0;
				 a=(int [])input.readObject();// array dei posti da disdire 				 
				 boolean trovato=true;
				 
				 for(i=0;i<a.length;i++){
					 if((a[i]!=0	&&	a[i]!=-1)){		 
					 if(trovato!=false){
							 for(j=0;j<prenotazioni.length;j++){								
									 if(prenotazioni[j]==a[i]){		
										 trovato=true;
										 
										 occupato[prenotazioni[j]]=null;
										 
										 break;
									 }
									 else if(prenotazioni[j]!=a[i]	&&prenotazioni[j]==-1){
										 trovato=false;
										 break;
									 }
							 }
					 }
					 else
						 break;
					 }					 
				 }
				
				 
				
				 if(trovato==true){
					 output.writeObject("annullamento confermato grazie");
					/* for(i=0;i<occupato.length;i++)
						 occupato[i]=null;*/
					 
				 }
				 else
					 output.writeObject("non hai prenotato questi posti");
				 break;
			 case 5:
				 stop=true;
				 break;
			 }
				
				
				
			 }
			 System.out.println("vuoi chiedere il server ? si/no");
			 decisione=tastiera.nextLine();
			 
			 if(decisione.equals("si")){
				 termina=true;
				 
				 System.out.println("chiusura connessione server");
					try {					
					c.close(); 				
					} catch (IOException e) {
						e.printStackTrace();
					}			
			 }
			 else
				 stop=false;
			 
			 
			 
		}while(termina==false);
	} catch (IOException e) {
		e.printStackTrace();
	}
	catch (ClassNotFoundException e) {
		e.printStackTrace();
	}
		 
			 
			 
	finally{
		if(decisione.equals("no")){
		System.out.println("chiusura connessione server");
		
		try {
			
			c.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if(output != null) output.close();
			
		}
		
		catch( IOException ioException ) {
			ioException.printStackTrace();
		}   
		

		try {
			if(input != null)input.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
		
	}

}
	
	
	
	public static void main(String[] args) {
		new Server();
	}
}
