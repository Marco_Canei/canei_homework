package Incrocio;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class GUI extends JPanel {
	public Macchina m[]=new Macchina[100];
	public final static int NORD=0;
	public final static int SUD=1;
	public final static int EST=2;
	public final static int OVEST=3;
	public GUI(){
	Timer t=new Timer(50, new ActionListener() {			
		@Override
		public void actionPerformed(ActionEvent e) {
			repaint();	
		}
	});
		t.setRepeats(true);
	    t.start();		
}

	@Override
	protected void paintComponent(Graphics e) {
		super.paintComponent(e);
		//------linee verticali-------
				e.drawLine(180, 0, 180, 180);
				e.drawLine(220, 0, 220, 180);
				e.drawLine(180, 220, 180, 400);
				e.drawLine(220, 220, 220, 400);
		//------linee orizzontali-----
				e.drawLine(0, 180, 180, 180);
				e.drawLine(0, 220, 180, 220);
				e.drawLine(220, 180, 400, 180);
				e.drawLine(220, 220, 400, 220);	
				
				for(int i=0;i<100;i++){
					if(m[i]!=null){
					switch(m[i].posizione){
					case NORD:			
						e.setColor(Color.GREEN);
						e.fillRect(m[i].x, m[i].y, 10, 20);	
					break;
					case SUD:
						e.setColor(Color.RED);
						e.fillRect(m[i].x, m[i].y, 10, 20);	
						break;
					case EST:		
						e.setColor(Color.YELLOW);
						e.fillRect(m[i].x, m[i].y, 20, 10);
						break;
					case OVEST:	
						e.setColor(Color.CYAN);
						e.fillRect(m[i].x, m[i].y, 20, 10);	
						break;
					}
				}
			}
	}
	
	public void add(Macchina m){
		for(int i=0;i<100;i++){
			if(this.m[i]==null){
				this.m[i]=m;
				break;
			}
		}	
	}
}
