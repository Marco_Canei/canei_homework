package Incrocio;

public class Macchina extends Thread {
	public int x,y,j;
	public int posizione;
	public Incrocio i;
	public final int NORD=0;
	public final int SUD=1;
	public final int EST=2;
	public final int OVEST=3;
	public Macchina(int posizione,Incrocio i){
		this.posizione=posizione;
		this.i=i;
		if(this.posizione==NORD){		
			y=0;
			x=185;
		}
		else if(this.posizione==SUD){
			y=400;	
			x=205;
		}
		else if(this.posizione==EST){
			y=185;
			x=400;
		}
		else {
			y=205;
			x=0;
		}	
	}
	@Override
	public void run() {
		for(j=0;j<=420;j+=1){
			try {
				sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			switch(posizione){
			case NORD:
				y++;
				if(y==160	&& y<=180){
					i.passa(this);			
				}
				else if(y==220){
					i.libera(this);
				}
			break;
			case SUD:
				y--;
				if(y==220	&&	y>=200){				
					i.passa(this);
				}
				else if(y==160){	
					i.libera(this);
				}
			break;
			case EST:	
				x--;
				
				if(x==220	&&	x>=200){
					i.passa(this);
				}
				else if(x==160){
					i.libera(this);
				}
			break;
			case OVEST:	
				x++;
				if(x==160	&& x<=180){
					i.passa(this);
				}
				else if(j==220){
					i.libera(this);
				}
			break;
			}
		}
	}
	
}
