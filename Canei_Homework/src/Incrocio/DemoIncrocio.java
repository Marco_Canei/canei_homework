package Incrocio;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

public class DemoIncrocio extends Thread{
	public final static int NORD=0;
	public final static int SUD=1;
	public final static int EST=2;
	public final static int OVEST=3;
	public static void main(String[] args) {
		JFrame f=new JFrame();
		Incrocio i=new Incrocio();
		GUI g=new GUI();

		Macchina m1=new Macchina((int)(Math.random()*4),i);
		m1.start();
		i.add(m1);
		g.add(m1);
		i.incrementa();
		
		Timer t=new Timer(1500, new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
						
					Macchina mr=new Macchina((int)(Math.random()*4),i);
					mr.start();
					i.add(mr);
					g.add(mr);
					i.incrementa();			
			}
		});
			t.setRepeats(true);
		    t.start();		
		    
		f.add(g);
		f.setVisible(true);
		f.setSize(450, 450);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

}
