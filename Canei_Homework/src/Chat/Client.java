package src.Chat;

// Fig. 18.5: Client.java
// Client that reads and displays information sent from a Server.
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class Client extends JFrame {
   private JTextField enterField;
   private JTextArea displayArea;
   private ObjectOutputStream output;
   private ObjectInputStream input;
   private String message = "";
   private String chatServer;
   private Socket connection;
   private int ID;

   public Client( String host ){
      super( "Client" );
      chatServer = host; // set server to which this client connects
      Container container = getContentPane();
      enterField = new JTextField();
      enterField.setEditable( false );
      enterField.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent event ){
               sendData( event.getActionCommand() );
               enterField.setText( "" );
            }
         }  
      ); 
      container.add( enterField, BorderLayout.NORTH );
      displayArea = new JTextArea();
      container.add( new JScrollPane( displayArea ),
         BorderLayout.CENTER );
      setSize( 300, 150 );
      setVisible( true );
   } // end Client constructor
   private void runClient() {
      try {
         connectToServer(); // Step 1: Create a Socket to make connection
         getStreams();      // Step 2: Get the input and output streams
         processConnection(); // Step 3: Process connection
      }
      catch ( EOFException eofException ) {
         System.err.println( "Client terminated connection" );
      }
      catch ( IOException ioException ) {
         ioException.printStackTrace();
      }
      finally {
         closeConnection(); // Step 4: Close connection
      }

   } // end method runClient
   private void connectToServer() throws IOException  {      
      displayMessage( "Attempting connection\n" );
      connection = new Socket(InetAddress.getByName("localhost"), 1234 );
      displayMessage( "Connected to: " + 
    		  connection.getInetAddress().getHostName() );
   }
   private void getStreams() throws IOException {
      output = new ObjectOutputStream( connection.getOutputStream() );     
      output.flush(); // flush output buffer to send header information    
      input=new ObjectInputStream(connection.getInputStream());    
      displayMessage( "\nGot I/O streams\n" );
   }
   
   private void processConnection() throws IOException{
      setTextFieldEditable( true );
      do { // process messages sent from server
         try {
        	 ID=(int) input.readObject();
            message = ( String ) input.readObject();
            displayMessage( "\n<Client"+ID+" Says> " + message );
         }
          catch ( ClassNotFoundException classNotFoundException ) {
            displayMessage( "\nUnknown object type received" );
         }
      } while ( !message.equals( "SERVER>>> TERMINATE" ) );
   } 
   
   private void closeConnection() {
      displayMessage( "\nClosing connection" );
      setTextFieldEditable( false ); // disable enterField
      try {
         output.close();
         input.close();
         connection.close();
      }
      catch( IOException ioException ) {
         ioException.printStackTrace();
      }
   }
   private void sendData( String message ) {
      try {
         output.writeObject( message );
         output.flush();
         displayMessage( "\n<You Wrote> " + message );
      }      // process problems sending object
      catch ( IOException ioException ) {
         displayArea.append( "\nError writing object" );
      }
   }
   private void displayMessage( final String messageToDisplay ) {    
      SwingUtilities.invokeLater(
         new Runnable() {  // inner class to ensure GUI updates properly
            public void run(){ // updates displayArea        
               displayArea.append( messageToDisplay );
               displayArea.setCaretPosition( 
                  displayArea.getText().length() );
            }
         }  // end inner class
      ); // end call to SwingUtilities.invokeLater
   }
   private void setTextFieldEditable( final boolean editable ){
      SwingUtilities.invokeLater(
         new Runnable() {  // inner class to ensure GUI updates properly
            public void run(){  // sets enterField's editability          
               enterField.setEditable( editable );
            }
         }  // end inner class
      ); // end call to SwingUtilities.invokeLater
   }
   public static void main( String args[] ){
      Client application;
      if ( args.length == 0 )
         application = new Client( "127.0.0.1" );
      else
         application = new Client( args[ 0 ] );
      application.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      application.runClient();
   }
} 

/**************************************************************************
 * (C) Copyright 1992-2003 by Deitel & Associates, Inc. and               *
 * Prentice Hall. All Rights Reserved.                                    *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
