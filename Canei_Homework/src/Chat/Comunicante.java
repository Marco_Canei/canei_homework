package src.Chat;

import java.io.*;
import java.net.*;
	
public class Comunicante extends Thread {
	private ObjectOutputStream output[]=new ObjectOutputStream[10];// array con gli stream di tutti i client creati 
	private Socket client; //il client a cui si riferisce il thread
	private int i=0,ID; //identificatore del client
	private String message;//messaggio da inviare a tutti i client
	private ObjectInputStream input;//stream di input per leggere il messaggio da inoltrare 
	
	public Comunicante(Socket client, ObjectOutputStream[] output, int ID) {	//costruttore
		this.client=client;
		this.output=output;
		this.ID=ID;
		try {
			input=new ObjectInputStream(client.getInputStream());	// ricavato lo stream di input del client
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void run() {
		super.run();
		while(true){	//ciclo infinito di lettura e scrittura
		read();
		send();
		}
	}
		
	
	public void read(){
		try {
			message=(String)input.readObject();			 
		} catch (IOException e) {
			e.printStackTrace();
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void send(){
		synchronized (output){
			for(i=0;i<output.length;i++){
				if(i!=ID	&&	output[i]!=null){
					try {
						output[i].writeObject(ID);
						output[i].writeObject(message);
						output[i].flush();
						} catch (IOException e) {
						e.printStackTrace();
						}
					}
				}
			}	//fine blocco sincronizzato 	
		}	//fine della procedura send 
	}	//fine classe Comunicante
