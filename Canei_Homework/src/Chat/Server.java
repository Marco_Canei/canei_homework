package src.Chat;

import java.io.*;
import java.net.*;

public class Server {
	private ServerSocket server;
	private Socket clients[]=new Socket[10];	//array che si popola man mano con i clients che si connettono 
	private ObjectOutputStream output[]=new ObjectOutputStream[10];//array con stream di output dei vari client
	private int i=0,ID;	//contatore e identificatore per il client
	
	private Comunicante connections[]=new Comunicante[10];//array dei thread per i vari client
	
	
	public Server(){
		 try {
			server=new ServerSocket(1234,10);	
		 System.out.println("server in attesa");
		 while(true){			 
			 	if(i<10){	
			 		ID=i;
			 		clients[i]=server.accept();	//attesa di nuovo client
			 		output[i]=new ObjectOutputStream(clients[i].getOutputStream());//ricavare stream di output
			 		connections[i]=new Comunicante(clients[i],output,ID);// nuovo thread per comunicazione
					connections[i].start();			 		
			 		i++;
			 	}
			 } 
		 }catch (IOException e) {
					e.printStackTrace();
				}				 	
		}
	
	
	public static void main(String[] args) {
		new Server();
		} 
	}

