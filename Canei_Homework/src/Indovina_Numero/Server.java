package Indovina_Numero;

import java.io.*;
import java.net.*;
import java.util.Random;

public class Server {
	private ServerSocket server;
	private Socket connessione;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private int n, tentativi=1;
	private Random r;
	boolean trovato=false;
	public Server(){
		r=new Random();
		try {
			server=new ServerSocket(12345);
			System.out.println("server in attesa");
			connessione=server.accept();
			
			output=new ObjectOutputStream(connessione.getOutputStream());
			input=new ObjectInputStream((connessione.getInputStream()));
			n=r.nextInt(100);
			System.out.println(n);
			
			
			
			while(tentativi<=3 && trovato!=true){
				String string =(String)input.readObject();
				
				if(string.equals(Integer.toString(n))){
					 output.writeObject("indovinato");
					 output.flush();
					 trovato=true;
				}
				else{
					output.writeObject("sbagliato riprova");
					output.flush();	
					tentativi++;
				}
			}
		}
			 catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			System.out.println("chiusura connessione server");
			try {
				
				connessione.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if(output != null) output.close();
			}
			catch( IOException ioException ) {
				ioException.printStackTrace();
			}   


			try {
				if(input != null)input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		
		
		
	}
	public static void main(String[] args) {
		new Server();
	}

}
