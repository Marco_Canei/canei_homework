package Indovina_Numero;

import java.io.*;
import java.net.*;

import javax.swing.JOptionPane;

public class Client {
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private Socket connessione;
	private int tentativi=1;
	private boolean trovato=false;
	public Client(){
		
		System.out.println("tentativo di connessione");
		try {
			connessione=new Socket(InetAddress.getByName("localhost"), 12345);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			output=new ObjectOutputStream(connessione.getOutputStream());
			output.flush();
			input=new ObjectInputStream(connessione.getInputStream());
			
			while(tentativi<=3 && trovato!=true	){
				
			String n=JOptionPane.showInputDialog("prova ad indovinare");
			output.writeObject(n);
			
				String risultato=(String)input.readObject();
				if(risultato.equals("indovinato"))
					trovato=true;
				
				JOptionPane.showMessageDialog(null,risultato);
				tentativi++;
			}
			
		}
			catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally{
			System.out.println("chiusura connessione");
			try {
				if(connessione != null) connessione.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if(output != null) output.close();
			}
			catch( IOException ioException ) {
				ioException.printStackTrace();
			}   


			try {
				if(input != null)input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}

	public static void main(String[] args) {
		new Client();
	}

}
