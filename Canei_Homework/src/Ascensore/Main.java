package Ascensore;
import javax.swing.*;
public class Main{
	
public static void main(String[] args) {
	JFrame f=new JFrame();
	GUI g=new GUI();
	Ascensore a=new Ascensore();
	Shared s=new Shared();
	
	JButton b1=new JButton("Piano terra");	
	b1.setName("Piano terra");
	JButton b2=new JButton("Primo piano");
	b2.setName("Primo piano");
	JButton b3=new JButton("Secondo piano");
	b3.setName("Secondo piano");
		
	g.setShared(s);
	a.setShared(s);
	
	a.start();

	b1.addActionListener(g);
	b2.addActionListener(g);
	b3.addActionListener(g);
	
	g.add(b1);
	g.add(b2);
	g.add(b3);
	f.add(g);
	f.setSize(500, 500);
	f.setVisible(true);
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}


}
