package Ascensore;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;


public class GUI extends JPanel implements ActionListener {
	private Ascensore a;
	private Shared s;
	public void setShared(Shared s) {
		this.s = s;
	}
	public GUI(){
		Timer t=new Timer(500, new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();	
			}
		});
			t.setRepeats(true);
		    t.start();		
	}
	@Override
	protected void paintComponent(Graphics e) {
		super.paintComponent(e);
		
		//piani 
		e.drawLine(100, 150, 500, 150);
		e.drawLine(100, 300, 500, 300);
		e.drawLine(100, 0, 100, 500);
		//ascensore 
		e.drawRect(16, s.y, 75, 146);	
	}

	@Override
	synchronized public void actionPerformed(ActionEvent e) {	
		if(((JButton)e.getSource()).getName()=="Piano terra")
			s.move("Piano terra");
		else if(((JButton)e.getSource()).getName()=="Primo piano")
			s.move("Primo piano");	
		else 
			s.move("Secondo piano");	
	}
}

